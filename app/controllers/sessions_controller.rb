class SessionsController < ApplicationController

  def create
    hash = request.env['omniauth.auth']
    session[:user_hash] = hash
    redirect_to root_path
  end

  def destroy
    session[:user_hash] = nil
    redirect_to root_path
  end
end
